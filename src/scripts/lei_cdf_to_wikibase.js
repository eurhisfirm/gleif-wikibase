import { assertValid, validateUrl } from "@biryani/core"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs"
import rpn from "request-promise-native"
import url from "url"
import xmlFlow from "xml-flow"

import pkg from "../../package.json"
import config from "../config"
import { cleanUpLine } from "../helpers"
import { objectsFromSqlResult, sleep } from "../helpers"
import { validateItemCore, validatePropertyCore } from "../validators/entities"
import { validateLeiRecord } from "../validators/lei"

let connection = null
let csrfToken = null
let headquartersLocationPropertyId = null
let legalLocationPropertyId = null
let postalCodePropertyId = null
let request = null
const wikibaseLanguages = ["en", "fr"]

// const corporateTitleItemIdById = {}
// let imagePropertyId = null
// let endTimePropertyId = null
// let positionHeldPropertyId = null
// let startTimePropertyId = null

const optionDefinitions = [
  {
    name: "xml-file",
    type: String,
    defaultOption: true,
    help: "Path of XML file containing LEI CDF",
  },
  {
    name: "lei-record",
    type: Number,
    help: "ID of LEI record to start resume from",
  },
]
const options = commandLineArgs(optionDefinitions)

async function login() {
  console.log("Login...")

  let result = await request.post(url.resolve(config.wikibase.url, "api.php"), {
    form: {
      action: "query",
      format: "json",
      meta: "tokens",
      type: "login",
    },
    json: true,
  })
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  // console.log(JSON.stringify(result, null, 2))
  const loginToken = result.query.tokens.logintoken

  result = await request.post(url.resolve(config.wikibase.url, "api.php"), {
    form: {
      action: "login",
      format: "json",
      lgname: config.wikibase.user,
      lgpassword: config.wikibase.password,
      lgtoken: loginToken,
    },
    json: true,
  })
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  // console.log(JSON.stringify(result, null, 2))
}

async function main() {
  request = rpn.defaults({
    headers: {
      "User-Agent": `${pkg.name}/${pkg.version} (${pkg.repository.url}; ${pkg.author})`,
    },
    jar: true,
  })

  await login()
  await requestCsrfToken()

  headquartersLocationPropertyId = (await upsertPropertyCore({
    datatype: "wikibase-item",
    descriptions: [
      {
        language: "en",
        value:
          "specific location where an organization's headquarters is or has been situated",
      },
      {
        language: "fr",
        value:
          "lieu où se trouve (ou où s'est trouvé) le siège principal d'une organisation",
      },
    ],
    labels: [
      {
        language: "en",
        value: "headquarters location",
      },
      {
        language: "fr",
        value: "lieu du siège social",
      },
    ],
  })).id
  console.log("headquarters location property:", headquartersLocationPropertyId)

  // Warning: invented property
  legalLocationPropertyId = (await upsertPropertyCore({
    datatype: "wikibase-item",
    descriptions: [
      {
        language: "en",
        value:
          "specific location where an organization's legal location is or has been situated",
      },
      {
        language: "fr",
        value:
          "lieu où se trouve (ou où s'est trouvé) le siège légal d'une organisation",
      },
    ],
    labels: [
      {
        language: "en",
        value: "legal location",
      },
      {
        language: "fr",
        value: "lieu légal",
      },
    ],
  })).id
  console.log("legal location property:", legalLocationPropertyId)

  postalCodePropertyId = (await upsertPropertyCore({
    datatype: "string",
    descriptions: [
      {
        language: "en",
        value:
          "identifier assigned by postal authorities for the subject area or building",
      },
      {
        language: "fr",
        value:
          "code postal/ZIP d'une entité dans le format spécifique d'un pays",
      },
    ],
    labels: [
      {
        language: "en",
        value: "postal code",
      },
      {
        language: "fr",
        value: "code postal",
      },
    ],
  })).id
  console.log("postal code property: ", postalCodePropertyId)

  // positionHeldPropertyId = (await upsertPropertyCore({
  //   datatype: "wikibase-item",
  //   descriptions: [
  //     {
  //       language: "en",
  //       value: "subject currently or formerly holds the object position or public office",
  //     },
  //     {
  //       language: "fr",
  //       value: "fonction occupée (politique, ecclésiastique, etc.)",
  //     },
  //   ],
  //   labels: [
  //     {
  //       language: "en",
  //       value: "position held",
  //     },
  //     {
  //       language: "fr",
  //       value: "fonction",
  //     },
  //   ],
  // })).id
  // console.log("position held property: ", positionHeldPropertyId)

  // startTimePropertyId = (await upsertPropertyCore({
  //   datatype: "time",
  //   descriptions: [
  //     {
  //       language: "en",
  //       value: "indicates the time a statement starts being valid",
  //     },
  //     {
  //       language: "fr",
  //       value: "qualificateur indiquant la date de début de validité pour une déclaration",
  //     },
  //   ],
  //   labels: [
  //     {
  //       language: "en",
  //       value: "start time",
  //     },
  //     {
  //       language: "fr",
  //       value: "date de début",
  //     },
  //   ],
  // })).id
  // console.log("start time property: ", startTimePropertyId)

  const xmlFile = fs.createReadStream(options["xml-file"])
  const xmlStream = xmlFlow(xmlFile)

  xmlStream.on("tag:lei:leirecord", async function (leiRecord) {
    // console.log(JSON.stringify(leiRecord, null, 2))
    leiRecord = assertValid(validateLeiRecord(leiRecord))
    await upsertLeiRecord(leiRecord)
  })
}

function numericIdFromItemId(itemId) {
  return parseInt(itemId.substring(1))
}

async function postToApi(body) {
  let result = null
  for (let delay of [
    1,
    2,
    4,
    8,
    16,
    32,
    64,
    128,
    256,
    512,
    1024,
    2048,
    4096,
    8192,
    16384,
    0,
  ]) {
    const form = {
      ...body,
      token: csrfToken,
    }
    result = await request.post(url.resolve(config.wikibase.url, "api.php"), {
      form,
      json: true,
    })
    if (result.error === undefined) {
      return result
    }
    if (
      result.error.messages &&
      result.error.messages.some(
        message =>
          message.name === "wikibase-validator-label-with-description-conflict",
      )
    ) {
      // Duplicate label: Let the caller handle this problem.
      return result
    }
    if (result.error.code === "badtoken") {
      // await logout()
      await login()
      await requestCsrfToken()
      continue
    }
    if (
      result.error.code === "failed-save" &&
      result.error.messages &&
      result.error.messages.some(
        message => message.name === "actionthrottledtext",
      )
    ) {
      console.log(
        "A failed-save error occurred:",
        JSON.stringify(result, null, 2),
      )
      console.log(`Sleeping ${delay} seconds...`)
      sleep(delay)
      continue
    }
    if (result.error.code === "no-external-page") {
      console.log(
        "A no-external-page error occurred:",
        JSON.stringify(result, null, 2),
      )
      console.log(`Sleeping ${delay} seconds...`)
      sleep(delay)
      continue
    }
    // Unhandled error. Throw an exception.
    break
  }
  assert(result.error === undefined, JSON.stringify(result, null, 2))
}

async function requestCsrfToken() {
  const result = await request.post(
    url.resolve(config.wikibase.url, "api.php"),
    {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  csrfToken = result.query.tokens.csrftoken
  assert(csrfToken !== undefined, JSON.stringify(result, null, 2))
  return csrfToken
}

async function upsertCity(city) {
  const item = await upsertItemCore({
    descriptions: [
      {
        language: "en",
        value: "City",
      },
      {
        language: "fr",
        value: "Ville",
      },
    ],
    labels: wikibaseLanguages.map(language => {
      return {
        language,
        value: city,
      }
    }),
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(city, item.id)

  return item.id
}

async function upsertCountry(country) {
  const item = await upsertItemCore({
    descriptions: [
      {
        language: "en",
        value: "Country",
      },
      {
        language: "fr",
        value: "Pays",
      },
    ],
    labels: wikibaseLanguages.map(language => {
      return {
        language,
        value: country,
      }
    }),
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(country, item.id)

  return item.id
}

async function upsertLeiRecord(leiRecord) {
  const leiEntity = leiRecord["lei:entity"]
  assert(leiEntity)

  const aliasesByLanguage = {}
  const labelByLanguage = {}

  const leiLegalName = leiEntity["lei:legalname"]
  assert(leiLegalName)
  const leiLegalNameString = leiLegalName.$text
  assert(leiLegalNameString)
  const leiLegalNameLanguage = leiLegalName.$attrs
    ? leiLegalName.$attrs["xml:lang"]
    : undefined
  if (leiLegalNameLanguage) {
    labelByLanguage[leiLegalNameLanguage] = leiLegalNameString
  }

  const leiOtherEntityNames = leiEntity["lei:otherentitynames"]
  for (let leiOtherEntityName of leiOtherEntityNames || []) {
    const attributes = leiOtherEntityName.$attrs
    assert(attributes)
    const language = attributes["xml:lang"]
    if (language) {
      let aliasesForLanguage = aliasesByLanguage[language]
      if (!aliasesForLanguage) {
        aliasesForLanguage = new Set()
      }
      aliasesForLanguage.add(leiOtherEntityName.$text)
    } else {
      for (let wikibaseLanguage of wikibaseLanguages) {
        if (!labelByLanguage[wikibaseLanguage]) {
          labelByLanguage[wikibaseLanguage] = leiOtherEntityName.$text
        }
      }
    }
    if (attributes.type === "ALTERNATIVE_LANGUAGE_LEGAL_NAME") {
      assert(language)
      if (!labelByLanguage[language]) {
        labelByLanguage[language] = leiOtherEntityName.$text
      }
    }
  }

  const leiTransliteratedOtherEntityNames =
    leiEntity["lei:transliteratedotherentitynames"]
  for (let leiTransliteratedOtherEntityName of leiTransliteratedOtherEntityNames ||
    []) {
    const attributes = leiTransliteratedOtherEntityName.$attrs
    assert(attributes)
    const language = attributes["xml:lang"]
    if (language) {
      let aliasesForLanguage = aliasesByLanguage[language]
      if (!aliasesForLanguage) {
        aliasesForLanguage = new Set()
      }
      aliasesForLanguage.add(leiTransliteratedOtherEntityName.$text)
    } else {
      for (let wikibaseLanguage of wikibaseLanguages) {
        if (!labelByLanguage[wikibaseLanguage]) {
          labelByLanguage[wikibaseLanguage] =
            leiTransliteratedOtherEntityName.$text
        }
      }
    }
  }

  if (!leiLegalNameLanguage) {
    for (let wikibaseLanguage of wikibaseLanguages) {
      if (!labelByLanguage[wikibaseLanguage]) {
        labelByLanguage[wikibaseLanguage] = leiLegalNameString
      }
    }
  }

  const aliases = []
  for (let [language, aliasesForLanguage] of Object.entries(
    aliasesByLanguage,
  )) {
    if (wikibaseLanguages.includes(language)) {
      for (let alias of aliasesForLanguage) {
        if (labelByLanguage[language] !== alias) {
          aliases.push({
            language,
            value: alias,
          })
        }
      }
    }
  }
  const item = await upsertItemCore({
    aliases: aliases,
    descriptions: [
      {
        language: "en",
        value: "Legal entity",
      },
      {
        language: "fr",
        value: "Entité légale",
      },
    ],
    labels: Object.entries(labelByLanguage)
      .filter(([language, value]) => wikibaseLanguages.includes(language))
      .map(([language, value]) => {
        return {
          language,
          value,
        }
      }),
    sitelinks: [
      {
        site: config.wikibase.site,
        title: `lei/${leiRecord["lei:lei"]}`,
      },
    ],
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  for (let [address, locationPropertyId] of [
    [leiEntity["lei:headquartersaddress"], headquartersLocationPropertyId],
    [leiEntity["lei:legaladdress"], legalLocationPropertyId],
  ]) {
    assert(address)

    const city = address["lei:city"]
    assert(city)
    const cityItemId = await upsertCity(city)

    let locationClaim = (claims[locationPropertyId] || []).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datavalue.value["entity-type"] === "item" &&
        claim.mainsnak.datavalue.value.id === cityItemId,
    )[0]
    if (locationClaim === undefined) {
      // Create new publisher claim.
      const result = await postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: item.id,
        format: "json",
        property: locationPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify({
          "entity-type": "item",
          "numeric-id": numericIdFromItemId(cityItemId),
        }),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      const claim = result.claim
      assert(claim !== undefined, JSON.stringify(result, null, 2))
      locationClaim = claim
    }

    let postalCode = address["lei:postalcode"]
    if (postalCode) {
      if (
        !((locationClaim.qualifiers || {})[postalCodePropertyId] || []).some(
          qualifier =>
            qualifier.snaktype === "value" &&
            qualifier.datavalue.value === postalCode,
        )
      ) {
        // Create "postal code" qualifier.
        const result = await postToApi({
          action: "wbsetqualifier",
          baserevid: lastrevid,
          bot: true,
          claim: locationClaim.id,
          format: "json",
          property: postalCodePropertyId,
          snaktype: "value",
          // summary: "Adding qualifier",
          value: JSON.stringify(postalCode),
        })
        assert(result.error === undefined, JSON.stringify(result, null, 2))
        // console.log(JSON.  stringify(result, null, 2))
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      }
    }
  }

  console.log(leiRecord["lei:lei"], leiLegalNameString, item.id)

  return item.id
}

// Update or create an item core (without claims).
async function upsertItemCore(data) {
  data = assertValid(validateItemCore(data))
  return upsertValidEntityCore("item", data)
}

async function upsertPropertyCore(data) {
  data = assertValid(validatePropertyCore(data))
  return upsertValidEntityCore("property", data)
}

async function upsertValidEntityCore(entityType, data) {
  const firstLabel = { ...data.labels[0] }

  let entity = null
  let result = null
  if (data.sitelinks === undefined) {
    result = await postToApi({
      action: "wbsearchentities",
      format: "json",
      language: firstLabel.language,
      limit: 1,
      search: firstLabel.value,
      type: entityType,
    })
    if (
      result.search.length > 0 &&
      data.labels.map(label => label.value).includes(result.search[0].label)
    ) {
      result = await postToApi({
        action: "wbgetentities",
        format: "json",
        ids: result.search[0].id,
      })
      assert(result.entities)
      assert(Object.keys(result.entities).length === 1)
      entity = Object.values(result.entities)[0]
    }
  } else {
    const firstSiteLink = data.sitelinks[0]
    result = await postToApi({
      action: "wbgetentities",
      format: "json",
      sites: firstSiteLink.site,
      titles: firstSiteLink.title,
    })
    assert(result.entities)
    assert(Object.keys(result.entities).length === 1)
    entity = Object.values(result.entities)[0]
  }
  if (entity === null || entity.missing !== undefined) {
    // Wikibase entity is missing. Create it.
    for (let i = 0; ; i++) {
      result = await postToApi({
        action: "wbeditentity",
        data: JSON.stringify(data, null, 2),
        format: "json",
        new: entityType,
      })
      if (
        result.error &&
        result.error.messages &&
        result.error.messages.some(
          message =>
            message.name ===
            "wikibase-validator-label-with-description-conflict",
        )
      ) {
        // An entity with the same name already exists.
        data.labels[0].value = `${firstLabel.value} (${i + 2})`
        continue
      }
      break
    }
    entity = result.entity
  } else {
    // Check if existing entity needs to be updated.
    const dataChanges = {}
    if (data.aliases !== undefined) {
      if (
        data.aliases.some(dataAlias => {
          const entityAliases = entity.aliases[dataAlias.language]
          if (entityAliases === undefined) {
            return true
          }
          return !entityAliases.some(
            entityAlias => entityAlias.value === dataAlias.value,
          )
        })
      ) {
        // Some aliases in data are not present in entity. Add them all.
        dataChanges.aliases = data.aliases.map(alias => {
          alias.add = ""
          return alias
        })
      }
    }
    if (data.datatype !== undefined) {
      assert.strictEqual(
        data.datatype,
        entity.datatype,
        `Existing property ${JSON.stringify(entity, null, 2)} has datatype "${
          entity.datatype
        }" different from "${data.datatype}"`,
      )
    }
    if (data.descriptions !== undefined) {
      if (
        data.descriptions.some(description => {
          const entityDescription = entity.descriptions[description.language]
          return (
            entityDescription === undefined ||
            description.value !== entityDescription.value
          )
        })
      ) {
        // Some descriptions in data are missing from entity. Add them.
        dataChanges.descriptions = data.descriptions
      }
    }
    if (
      data.labels.some(label => {
        const entityLabel = entity.labels[label.language]
        return entityLabel === undefined || label.value !== entityLabel.value
      })
    ) {
      // Some labels in data are missing from entity. Add them.
      dataChanges.labels = data.labels
    }
    if (data.sitelinks !== undefined) {
      if (
        data.sitelinks.some(siteLink => {
          const entitySiteLink = entity.sitelinks[siteLink.site]
          return (
            entitySiteLink === undefined ||
            siteLink.title !== entitySiteLink.title
          )
        })
      ) {
        // Some sitelinks in data are missing from entity. Add them.
        dataChanges.sitelinks = data.sitelinks
      }
    }
    if (Object.keys(dataChanges).length > 0) {
      // Update entity.
      console.log(
        `Updating entity ${entity.id}:`,
        JSON.stringify(dataChanges, null, 2),
      )
      for (let i = 0; ; i++) {
        result = await postToApi({
          action: "wbeditentity",
          data: JSON.stringify(dataChanges, null, 2),
          format: "json",
          id: entity.id,
        })
        if (
          result.error &&
          result.error.messages &&
          result.error.messages.some(
            message =>
              message.name ===
              "wikibase-validator-label-with-description-conflict",
          )
        ) {
          if (dataChanges.labels !== undefined) {
            // An entity with the same name already exists.
            dataChanges.labels[0] = { ...data.labels[0] }
            dataChanges.labels[0].value = `${firstLabel.value} (${i + 2})`
            continue
          } else if (dataChanges.descriptions !== undefined) {
            // An entity with the same label & description already exists.
            dataChanges.labels = [...data.labels]
            dataChanges.labels[0] = { ...data.labels[0] }
            dataChanges.labels[0].value = `${firstLabel.value} (${i + 2})`
            continue
          }
        }
        break
      }
      entity = result.entity
    }
  }
  return entity
}

main().catch(error => {
  console.log(error.stack || error)
  process.exit(1)
})
