require("dotenv").config()

import { validateConfig } from "./validators/config"

const config = {
  wikibase: {
    url: process.env.WIKIBASE_URL || "https://wikibase.eurhisfirm.eu/",
    user: process.env.MEDIAWIKI_BOT_USERNAME || "MEDIAWIKI_BOT_USERNAME",
    password: process.env.MEDIAWIKI_BOT_PASSWORD || "MEDIAWIKI_BOT_USERNAME",
    site: process.env.MEDIAWIKI_SITELINK_SITE || "gleif.org",
  },
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(validConfig, null, 2)}\nError:\n${JSON.stringify(error, null, 2)}`
  )
  process.exit(-1)
}

export default validConfig
