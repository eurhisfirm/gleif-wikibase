import {
  validateArray,
  validateChain,
  validateChoice,
  validateEmptyToNull,
  validateFunction,
  validateMissing,
  validateNonEmpty,
  validateNonEmptyTrimmedString,
  validateOption,
  validateStrictEqual,
  validateString,
  validateTest,
} from "@biryani/core"

import iso6391 from "iso-639-1"

const leiRegexp = /^[0-9A-Z]{20}$/

function validateLeiAddress(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "$attrs"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      validateLeiAddressAttributes,
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:additionaladdressline"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateFunction(value => (Array.isArray(value) ? value : [value])),
        validateArray(
          validateChain(
            validateString,
            validateFunction(value =>
              value
                .split(/\s+/)
                .join(" ")
                .trim(),
            ),
            validateEmptyToNull(null),
          ),
        ),
      ],
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of [
    "lei:addressnumber",
    "lei:addressnumberwithinbuilding",
    "lei:mailrouting",
    "lei:postalcode",
    "lei:region",
  ]) {
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateString,
        validateFunction(value =>
          value
            .split(/\s+/)
            .join(" ")
            .trim(),
        ),
        validateEmptyToNull(null),
      ],
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["lei:city", "lei:country", "lei:firstaddressline"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiAddressAttributes(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "xml:lang"
    remainingKeys.delete(key)
    const [value, error] = validateOptionalIso6391LanguageCode(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiEntity(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "lei:headquartersaddress"
    remainingKeys.delete(key)
    const [value, error] = validateLeiAddress(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:legaladdress"
    remainingKeys.delete(key)
    const [value, error] = validateLeiAddress(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:legalname"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateFunction(value =>
        typeof value === "string" ? { $text: value } : value,
      ),
      validateLeiLegalName,
    )(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:otherentitynames"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateFunction(value => (Array.isArray(value) ? value : [value])),
        validateArray(validateLeiOtherEntityName),
      ],
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:transliteratedotherentitynames"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [
        validateFunction(value => (Array.isArray(value) ? value : [value])),
        validateArray(validateLeiTransliteratedOtherEntityName),
      ],
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  // TODO

  // for (let key of remainingKeys) {
  //   errors[key] = "Unexpected item"
  // }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiLegalName(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "$attrs"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      validateLeiLegalNameAttributes,
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "$text"
    remainingKeys.delete(key)
    const [value, error] = validateLeiLegalNameString(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiLegalNameAttributes(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "xml:lang"
    remainingKeys.delete(key)
    const [value, error] = validateOptionalIso6391LanguageCode(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

const validateLeiLegalNameString = validateChain(
  validateString,
  validateFunction(value =>
    value
      .split(/\s+/)
      .join(" ")
      .trim(),
  ),
  validateNonEmpty,
)

function validateLeiOtherEntityName(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "$attrs"
    remainingKeys.delete(key)
    const [value, error] = validateLeiOtherEntityNameAttributes(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "$text"
    remainingKeys.delete(key)
    const [value, error] = validateLeiLegalNameString(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiOtherEntityNameAttributes(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "type"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateString,
      validateChoice([
        "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
        "PREVIOUS_LEGAL_NAME",
        "TRADING_OR_OPERATING_NAME",
      ]),
    )(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "xml:lang"
    remainingKeys.delete(key)
    const [value, error] = validateOptionalIso6391LanguageCode(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiTransliteratedOtherEntityName(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "$attrs"
    remainingKeys.delete(key)
    const [value, error] = validateLeiTransliteratedOtherEntityNameAttributes(
      object[key],
    )
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "$text"
    remainingKeys.delete(key)
    const [value, error] = validateLeiLegalNameString(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiTransliteratedOtherEntityNameAttributes(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "type"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateString,
      validateChoice([
        "AUTO_ASCII_TRANSLITERATED_LEGAL_NAME",
        "PREFERRED_ASCII_TRANSLITERATED_LEGAL_NAME",
      ]),
    )(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "xml:lang"
    remainingKeys.delete(key)
    const [value, error] = validateOptionalIso6391LanguageCode(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

export function validateLeiRecord(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "$attrs"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      validateLeiRecordAttributes,
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "$name"
    remainingKeys.delete(key)
    const [value, error] = validateStrictEqual("lei:leirecord")(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:entity"
    remainingKeys.delete(key)
    const [value, error] = validateLeiEntity(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:extension"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateString])(
      object[key],
    )
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:lei"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateString,
      validateTest(value => leiRegexp.test(value), "Invalid LEI code"),
    )(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "lei:registration"
    remainingKeys.delete(key)
    const [value, error] = validateLeiRegistration(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiRecordAttributes(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "xmlns:ext"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      validateStrictEqual("http://lei.es.extension/2014"),
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "xmlns:leifr"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      validateStrictEqual("http://xml.lei-france.fr/schema/2014/"),
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

function validateLeiRegistration(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  // TODO

  // for (let key of remainingKeys) {
  //   errors[key] = "Unexpected item"
  // }
  if (Object.keys(errors).length !== 0) {
    return [object, errors]
  }
  for (let [key, value] of Object.entries(object)) {
    if (value === null) {
      delete object[key]
    }
  }
  if (Object.keys(object).length === 0) {
    return [null, null]
  }
  return [object, null]
}

const validateOptionalIso6391LanguageCode = validateOption([
  validateMissing,
  [
    validateString,
    validateFunction(value => {
      value = value.toLowerCase().split("-")[0]
      switch (value) {
        case "":
          return null
        case "gb":
          // english
          return "en"
        case "gr":
          // greek
          return "el"
        default:
          return value
      }
    }),
    validateEmptyToNull(
      validateTest(
        value =>
          ["me" /* Montenegro */, "rs" /* Serbia */].includes(value)
            ? true
            : iso6391.validate(value),
        "Invalid ISO-6391 language code",
      ),
    ),
  ],
])
