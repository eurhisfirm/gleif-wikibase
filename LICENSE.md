# GLEIF-Wikibase

_Wikibase bots for the "Global Legal Entity Identifier Foundation (GLEIF)" data_

By: Emmanuel Raviart <emmanuel@raviart.com>

Copyright (C) 2018 École d’économie de Paris (PSE)

https://gitlab.huma-num.fr/eurhisfirm/gleif-wikibase

> GLEIF-Wikibase is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> GLEIF-Wikibase is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
