# GLEIF-Wikibase

_Wikibase bots for the "Global Legal Entity Identifier Foundation (GLEIF)" data_

## Install

```bash
git clone https://gitlab.huma-num.fr/eurhisfirm/gleif-wikibase.git
cd gleif-wikibase/
```

Edit `src/config.js` to change database informations. Then

```bash
npm install
```

## Update Wikibase with GLEIF issuers

```bash
node_modules/.bin/babel-node src/scripts/generate_wikibase_issuers.js
```
